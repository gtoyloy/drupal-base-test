<?php

/**
 * @file
 * Custom theme hook implementations.
 */

/**
 * Implements hook_theme().
 */
function oie_theme_theme($existing, $type, $theme, $path): array {
  /** @var \Drupal\Core\Extension\ThemeExtensionList $extensionList */
  $extensionList = \Drupal::service('extension.list.theme');
  $path = $extensionList->getPath('oie_theme') . '/templates';

  return [
    'icon' => [
      'variables' => [
        'name' => NULL,
        'classes' => [],
        'extra' => NULL,
      ],
      'path' => $path . '/components',
    ],
  ];
}
