<?php

/**
 * @file
 * Preprocess hook implementations.
 */

use Drupal\block\Entity\Block;

/**
 * Implements hook_preprocess_HOOK().
 */
function oie_theme_preprocess_html(array &$variables): void {
  /** @var \Drupal\Core\Extension\ThemeExtensionList $themeExtensionList */
  $themeExtensionList = \Drupal::service('extension.list.theme');
  $icons = file_get_contents($themeExtensionList->getPath('oie_theme') . '/dist/images/icons.svg');
  $variables['page_bottom']['icons'] = [
    '#type' => 'inline_template',
    '#template' => '<span class="d-none icons-sprite">' . $icons . '</span>',
  ];
}

/**
 * Implements hook_preprocess_page().
 */
function oie_theme_preprocess_page(&$variables) {
  // Fix for toolbar changing body padding.
  if (\Drupal::currentUser()->hasPermission('access toolbar')) {
    $variables['#attached']['library'][] = 'oie_theme/toolbar';
  }
}

/**
 * Implements hook_preprocess_image().
 */
function oie_theme_preprocess_image(&$vars) {
  $vars['attributes']['class'][] = 'img-fluid';
}

/**
 * Implements hook_preprocess_table().
 */
function oie_theme_preprocess_table(&$variables) {
  $variables['attributes']['class'][] = 'table';
}

/**
 * Implements hook_preprocess_table().
 */
function oie_theme_preprocess_views_view_table(&$variables) {
  $variables['attributes']['class'][] = 'table';
  $variables['attributes']['class'][] = 'table-striped';
}

/**
 * Implements hook_preprocess_input().
 */
function oie_theme_preprocess_input(&$variables) {
  if ($variables['element']['#type'] === 'submit') {

    if ($variables['element']['#value'] === 'Preview') {
      $variables['attributes']['class'][] = 'btn-secondary';
    }

    if (!in_array('btn', $variables['attributes']['class'])) {
      $variables['attributes']['class'][] = 'btn';
    }

    if (empty(array_intersect([
      'btn-primary',
      'btn-primary-lighter',
      'btn-secondary',
      'btn-success',
      'btn-warning',
      'btn-danger',
      'btn-link',
      'btn-info',
    ], $variables['attributes']['class']))) {
      $variables['attributes']['class'][] = 'btn-secondary';
    }
  }
}

/**
 * Implements hook_preprocess_block().
 */
function oie_theme_preprocess_block(&$variables) {
  if (isset($variables['elements']['#id'])) {
    $region = Block::load($variables['elements']['#id'])->getRegion();
    if (!empty($region)) {
      $variables['content']['#attributes']['data-block']['region'] = $region;
    }
  }
}
