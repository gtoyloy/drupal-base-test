(() => {
  const header = document.getElementsByTagName("header")[0];
  const body = document.getElementsByTagName("body")[0];

  // When the admin toolbar is changed, update our sticky header offset.
  const observer = new MutationObserver((mutations) => {
    const newPadding = mutations[0].target.style.paddingTop || 0;
    header.style.top = newPadding;
  });

  // Event listeners.
  document.addEventListener("DOMContentLoaded", () => {
    observer.observe(body, { attributes: true, attributeFilter: ["style"] });
  });

  window.addEventListener("load", () => {
    header.style.top = body.style.paddingTop || "0";
  });
})();
