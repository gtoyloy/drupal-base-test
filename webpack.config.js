const WebpackBar = require("webpackbar");
const path = require("path");
const glob = require("glob");
const globConcat = require("glob-concat");
const globImporter = require("node-sass-glob-importer");
const FileManagerPlugin = require("filemanager-webpack-plugin");
const SpriteLoaderPlugin = require("svg-sprite-loader/plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = (env, argv) => {
  const entries = {};
  let relativeDir = "./";
  let componentsThemeDir = "../../..";

  // Support watching all componenets. @todo make this better.
  if (env.components) {
    relativeDir = "./*/";
    componentsThemeDir = "../..";
  }

  // Defines entry point names as the output directory/filename.
  glob.sync(`${relativeDir}assets/images/*.svg`).forEach((entry) => {
    entries[`images/${path.basename(entry)}`] = [entry];
  });
  glob.sync(`${relativeDir}assets/icons/*.svg`).forEach((entry) => {
    entries[`images/${path.basename(entry)}`] = [entry];
  });
  glob.sync(`${relativeDir}assets/js/*.js`).forEach((entry) => {
    entries[`js/${path.basename(entry).replace(/\.[\w]+$/, "")}`] = [entry];
  });
  glob.sync(`${relativeDir}/assets/scss/*.scss`).forEach((entry) => {
    entries[`css/${path.basename(entry).replace(/\.[\w]+$/, "")}`] = [entry];
  });

  if (!Object.keys(entries).length) {
    process.exit(0);
  }

  return {
    resolve: {
      alias: {
        "theme": path.resolve(componentsThemeDir, "./assets/scss/abstracts"),
      },
    },
    mode: argv.mode ? argv.mode : "development",
    devtool: argv.mode !== "production" ? "source-map" : false,
    stats: "errors-only",
    infrastructureLogging: {
      colors: true,
      level: "log",
    },
    watchOptions: {
      poll: 1000,
    },
    entry: entries,
    module: {
      rules: [
        {
          test: /\.svg/,
          include: /assets\/images/,
          type: "asset/resource",
          use: ["svgo-loader"],
        },
        {
          test: /\.svg/,
          include: /assets\/icons/,
          use: [
            {
              loader: "svg-sprite-loader",
              options: {
                extract: true,
                outputPath: "images/",
                spriteFilename: "icons.svg",
              },
            },
            "svgo-loader",
          ],
        },
        {
          test: /\.scss$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: "css-loader", // stop url() from being processed
              options: {
                sourceMap: true,
                url: false,
              },
            },
            {
              loader: "resolve-url-loader", // resolve relatively referenced assets
              options: {
                sourceMap: true,
              },
            },
            {
              loader: "postcss-loader",
              options: {
                sourceMap: true,
              },
            },
            {
              loader: "sass-loader",
              options: {
                sourceMap: true,
                sassOptions: {
                  importer: globImporter(),
                  includePaths: globConcat.sync([]),
                },
              },
            },
          ],
        },
        {
          test: /\.js$/,
          include: /assets\/js/,
          use: [
            {
              loader: "babel-loader",
              options: {
                presets: [["@babel/preset-env", { modules: "commonjs" }]],
                cacheDirectory: true,
              },
            },
          ],
        },
      ],
    },
    optimization: {
      removeAvailableModules: false,
    },
    output: {
      path: path.resolve(".", "dist"),
      publicPath: "/dist/",
      assetModuleFilename: "images/[name][ext][query]",
    },
    plugins: [
      new WebpackBar({
        name: `Directory: .${path.resolve(".").replace(__dirname, "")}`,
      }),
      new FileManagerPlugin({
        runOnceInWatchMode: true,
        events: {
          onStart: {
            delete: ["./dist"],
          },
          onEnd: {
            delete: ["./dist/css/*.js*", "./dist/images/*.js*"],
          },
        },
      }),
      new SpriteLoaderPlugin({ plainSprite: true }),
      new MiniCssExtractPlugin(),
    ],
  };
};
