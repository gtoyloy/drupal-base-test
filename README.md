# &lt;PROJECT NAME&gt;

This is a base template for use with SWAT, if you are unsure what that means, you probably don't need this repository ;)

A description of the project and it's deliverables here, along with the existing site URLS.

## ☁️ Hosting

Details about where the site is hosted and any special considerations.

### Environments

| Environment                                         | Branch    | Main purpose  | Description                           |
|-----------------------------------------------------|-----------|---------------|---------------------------------------|
| [QA](http://project.tugboatqa.com/)                 | feature/X | ImageX QA     | Tugboat env, QA on PR's (optional).   |
| [DEV](http://project.prod.acquia-sites.com/)        | master    | ImageX DEV    | Contains code approved by Lead Dev.   |
| [STAGE](http://project.prod.acquia-sites.com/)      | stage     | Client UAT    | Contains code approved on DEV by QA.  |
| [PRODUCTION](http://project.prod.acquia-sites.com/) | prod      | Content entry | Code and Database/Content that will be launched

## 🔥 DEVELOPMENT

Brief explaination of development special considerations.

#### Workflow

1. Developer works on a feature branch created from master
2. Developer create a Pull Request from feature-branch -> master
3. ....
4. ...
5. ..
6. .

#### Getting Started

List out the commands needed for a developer to start this project. (i.e. clone xx, fin init, etc)

#### Full remote sync

Run this command to sync the sanitized remote database, files, and configuration:

```shell
fin sync
```

### 🛠 Integrations and Custom Bits

Describe anything of note that a developer might find useful.

```shell
drush my:command --isgreat
```

### 🎨 Frontend

#### Node package management

All package dependencies should be installed in the root project directory. Webpack ensures all themes
and modules have direct access to the installed packages.

#### Build frontend

> **IMPORTANT:** Set `{ minified: true }` on custom library assets because Webpack handles minification internally.

```shell
fin frontend/build [type:name|optional] # Build frontend components.
fin frontend/watch [type:name|optional] # Watch and build frontend components in realtime.
fin blt frontend # Build frontend components in production mode.
```

Remove or replace `[type:name|optional]` with either `modules:module_name` or `themes:theme_name`.

```shell
fin yarn add [package] # Add new package.
fin yarn remove [package] # Remove existing package.
```

#### ES6 JavaScript

Write JavaScript using ES6 and place into the custom theme or modules `assets/js/` directory. Webpack automatically
transpiles and saves the output into the `dist/js/` directory.

#### Images and Icons

Place images into the custom theme or modules `assets/images/` directory. Place SVG icons into the `assets/icons/`
directory. SVGs are automatically optimized with SVGO. Icons are saved to `dist/images/icons.svg` as a sprite. All
other images are saved to `dist/images/` with their original filename.

Using Twig to insert SVG icons:

```twig
{% include '@theme_name/components/icon.html.twig' with {'name': 'icon_name', 'classes': ['icon-sm']} %}
```

### ✅ Validation

#### Linters

```shell
fin yarn lint # Run code linters.
fin yarn lint:fix # Run code linters and automatically fix issues.
```

> **IMPORTANT:** Validation errors prevent remote pushes and cause CI/CD pipelines failures.

#### Spellcheck

```shell
fin yarn spellcheck # Run spellcheck.
fin yarn spellcheck:dictionary # Generate new project dictionary words.
```

> **IMPORTANT:** Generate project dictionary words only after running the spellchecker.
