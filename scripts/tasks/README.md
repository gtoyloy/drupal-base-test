# Node.js Task Dispatcher

Dispatches Node.js commands to the appropriate task handler script. See the root `package.json` for available tasks.
